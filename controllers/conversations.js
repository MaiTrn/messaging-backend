const { tokenExtractor } = require("../utils/middleware");

const conversationsRouter = require("express").Router();
const Conversation = require("../models/conversation");

//create a new conversation
conversationsRouter.post("/", tokenExtractor, async (req, res) => {
  const existingConversation = await Conversation.findOne({
    members: { $all: [req.user.id, req.body.receiver] },
  });

  if (existingConversation) {
    res.status(400).json({ error: "Conversation already exists" }).end();
    return;
  }

  const conversation = new Conversation({
    members: [req.user.id, req.body.receiver],
  });

  const newConversation = await conversation.save();
  res.status(200).json(newConversation);
});

//get conversations of a user
conversationsRouter.get("/", tokenExtractor, async (req, res) => {
  const conversations = await Conversation.find({
    members: { $in: [req.user.id] },
  });

  res.status(200).json(conversations);
});

module.exports = conversationsRouter;

