const bcrypt = require("bcrypt");
const usersRouter = require("express").Router();
const User = require("../models/user");
const { tokenExtractor } = require("../utils/middleware");

//sign up
usersRouter.post("/", async (req, res) => {
  const body = req.body;

  //check password
  if (!body.password) {
    return res.status(400).json({
      error: "User validation failed: `password` is required.",
    });
  }
  if (body.password.length < 3) {
    return res.status(400).json({
      error:
        "User validation failed: `password` is shorter than the minimum allowed length (3).",
    });
  }

  //hash password
  const passwordHash = await bcrypt.hash(body.password, 10);
  const user = new User({
    name: body.name,
    username: body.username,
    email: body.email,
    avatar: body.avatar,
    passwordHash,
  });

  const newUser = await user.save();
  res.json(newUser);
});

usersRouter.get("/", async (req, res) => {
  if (req.query.name) {
    const users = await User.find({
      name: new RegExp(`^${req.query.name}`, "i"),
    }).populate("contactList", {
      username: 1,
      name: 1,
      avatar: 1,
    });
    res.json(users);
    return;
  }

  const users = await User.find({}).populate("contactList", {
    username: 1,
    name: 1,
    avatar: 1,
  });
  res.json(users);
});

usersRouter.get("/:id", async (req, res) => {
  const user = await User.findById(req.params.id).populate("contactList", {
    username: 1,
    name: 1,
    avatar: 1,
  });
  res.json(user);
});

//add a user to contact list
usersRouter.put("/contacts", tokenExtractor, async (req, res) => {
  const contact = await User.findById(req.body.contactId);

  if (!contact) {
    res
      .status(400)
      .json({ error: "No user with the specified id exists" })
      .end();
    return;
  }

  const user = await User.findById(req.user.id);
  if (user.contactList.includes(contact.id)) {
    res.status(400).json({ error: "User already in contact list" }).end();
    return;
  }

  user.contactList = user.contactList.concat(contact.id);
  const updatedUser = await User.findByIdAndUpdate(req.user.id, user, {
    new: true,
  }).populate("contactList", {
    username: 1,
    name: 1,
    avatar: 1,
  });

  if (updatedUser) {
    res.status(200).json(updatedUser);
  } else res.status(400).end();
});

//change info
usersRouter.put("/", tokenExtractor, async (req, res) => {
  const user = {
    username: req.body.username,
    name: req.body.name,
    avatar: req.body.avatar,
  };

  const updatedUser = await User.findByIdAndUpdate(req.user.id, user, {
    new: true,
  });

  if (updatedUser) {
    res.status(200).json(updatedUser);
  } else res.status(400).end();
});

module.exports = usersRouter;
