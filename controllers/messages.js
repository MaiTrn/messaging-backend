const messagesRouter = require("express").Router();

const Message = require("../models/message");
const Conversation = require("../models/conversation");
const { tokenExtractor } = require("../utils/middleware");

messagesRouter.post("/", tokenExtractor, async (req, res) => {
  const conv = await Conversation.findById(req.body.conversationId);
  const message = new Message({ ...req.body, senderId: req.user.id });
  if (!conv.members.includes(req.user.id)) {
    res.status(400).json({ error: "User not in conversation" }).end();
    return;
  }
  const newMessage = await message.save();
  res.status(200).json(newMessage);
});

messagesRouter.get("/:conversationId", tokenExtractor, async (req, res) => {
  const messages = await Message.find({
    conversationId: req.params.conversationId,
  }).populate("senderId", { name: 1, username: 1 });
  res.status(200).json(messages);
});

module.exports = messagesRouter;
