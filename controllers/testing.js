const router = require("express").Router();
const Conversation = require("../models/conversation");
const User = require("../models/user");
const Message = require("../models/message");

router.post("/reset", async (request, response) => {
  await Conversation.deleteMany({});
  await User.deleteMany({});
  await Message.deleteMany({});

  response.status(204).end();
});

module.exports = router;
