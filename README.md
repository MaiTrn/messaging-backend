# MESSAGING REST API

Messaging backend is a RESTful API for handling users creation and sending messages.

## Environment variables

| Name             | Description                                       |
| ---------------- | ------------------------------------------------- |
| PORT             | Port number the server will run on. Default: 5000 |
| SECRET           | The secret used to hash password                  |
| MONGODB_URI      | MongoDB url for database connection               |
| TEST_MONGODB_URI | MongoDB url for testing                           |

## Pre-requisites

Install [Node.js](https://nodejs.org/en/)

## Getting started

- Clone the repository

```
git clone  git@gitlab.com:MaiTrn/messaging-backend.git messaging-backend
```

- Install dependencies

```
cd messaging-backend
npm install

```

- Build and run the project

```
npm start
```

Navigate to `http://localhost:5000`

- API Document endpoints

  swagger Endpoint : http://localhost:5000/api-docs

## Project Structure

The folder structure of this app is explained below:

| Name                  | Description                                                                                      |
| --------------------- | ------------------------------------------------------------------------------------------------ |
| **controllers**       | Controllers define functions to serve various express routes.                                    |
| **utils**             | Utility functions helping the app                                                                |
| **utils/middlewares** | Express middlewares which process the incoming requests before handling them down to the routes  |
| **models**            | Models define schemas that will be used in storing and retrieving data from database             |
| **tests**             | Tests for different endpoints of the application                                                 |
| **rest**              | REST queries                                                                                     |
| **docs**              | Swagger documentation files                                                                      |
| app.js                | Configurations for an express app                                                                |
| index.js              | Entry point to express app                                                                       |
| package.json          | Contains npm dependencies as well as [build scripts](#what-if-a-library-isnt-on-definitelytyped) |

## Testing

The tests are done using jest

```
 "jest": "^27.5.1",
```

## Starting the project

All the different build steps are orchestrated via [npm scripts](https://docs.npmjs.com/misc/scripts).
Npm scripts basically allow us to call (and chain) terminal commands via npm.

| Npm Script | Description                                                                                 |
| ---------- | ------------------------------------------------------------------------------------------- |
| `start`    | Runs the app in production mode. Can be invoked with `npm start`                            |
| `dev`      | Runs the app in development mode, enables hot reload on save. Can be invoked with `npm dev` |
| `test`     | Runs tests using jest                                                                       |

## Contributors

- Kim Tran - 1900309
- Mai Tran - 1900308

## License

[MIT](https://choosealicense.com/licenses/mit/)
