const express = require("express");
require("express-async-errors");

const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const swaggerUI = require("swagger-ui-express");

const config = require("./utils/config");
const middleware = require("./utils/middleware");
const usersRouter = require("./controllers/users");
const loginRouter = require("./controllers/login");
const conversationsRouter = require("./controllers/conversations");
const messagesRouter = require("./controllers/messages");
const docs = require("./docs");

mongoose
  .connect(config.MONGODB_URI)
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((error) => {
    console.log(`Error connecting to MongoDB: ${error.message}`);
  });
app.use(cors());
app.use(express.json());

if (process.env.NODE_ENV === "test") {
  const testingRouter = require("./controllers/testing");
  app.use("/api/testing", testingRouter);
}

app.use("/api/users", usersRouter);
app.use("/api/login", loginRouter);
app.use("/api/conversations", conversationsRouter);
app.use("/api/messages", messagesRouter);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(docs));

app.use(middleware.unknownEndpoint);
app.use(middleware.errorHandler);

module.exports = app;
