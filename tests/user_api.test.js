const bcrypt = require("bcrypt");
const User = require("../models/user");
const mongoose = require("mongoose");
const supertest = require("supertest");
const app = require("../app");
const api = supertest(app);

const usersInDb = async () => {
  const users = await User.find({});
  return users.map((u) => u.toJSON());
};

beforeEach(async () => {
  await User.deleteMany({});
  const passwordHash = await bcrypt.hash("sekret", 10);
  const user1 = new User({
    username: "testuser",
    name: "Test",
    email: "testemail@gmail.com",
    passwordHash,
  });

  const user2 = new User({
    username: "testanother",
    name: "Another",
    email: "test@gmail.com",
    passwordHash,
  });

  await user1.save();
  await user2.save();
});

describe("username must be unique", () => {
  test("creation succeeds with a fresh username", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "janeD",
      name: "Jane",
      email: "jane@gmail.com",
      password: "jane123",
    };

    await api
      .post("/api/users")
      .send(newUser)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length + 1);

    const usernames = usersAtEnd.map((u) => u.username);
    expect(usernames).toContain(newUser.username);
  });

  test("creation fails with status code 400 if username is taken", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "testuser",
      name: "Test",
      email: "testemail@gmail.com",
      password: "sekret",
    };

    const result = await api
      .post("/api/users")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toContain("`username` to be unique");

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });
});

describe("username and password must be given", () => {
  test("creation succeeds with both username and password given", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "janeD",
      name: "Jane",
      email: "jane@gmail.com",
      password: "jane123",
    };

    await api
      .post("/api/users")
      .send(newUser)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length + 1);

    const usernames = usersAtEnd.map((u) => u.username);
    expect(usernames).toContain(newUser.username);
  });

  test("creation fails with no username", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      name: "Jane",
      email: "jane@gmail.com",
      password: "jane123",
    };

    const result = await api
      .post("/api/users")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toContain("`username` is required");

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });

  test("creation fails with no password", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "janeD",
      name: "Jane",
      email: "jane@gmail.com",
    };

    const result = await api
      .post("/api/users")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toContain("`password` is required");

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });
});

describe("username must be at least 5 characters long and password must be at least 3 characters long", () => {
  test("creation succeeds if username is 5 characters long and password is 3 characters long", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "janeD",
      name: "Jane",
      email: "jane@gmail.com",
      password: "jane123",
    };

    await api
      .post("/api/users")
      .send(newUser)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length + 1);

    const usernames = usersAtEnd.map((u) => u.username);
    expect(usernames).toContain(newUser.username);
  });

  test("creation fails if username is less than 5 characters long", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "jane",
      name: "Jane",
      email: "jane@gmail.com",
      password: "jane123",
    };

    const result = await api
      .post("/api/users")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toContain(
      "shorter than the minimum allowed length"
    );

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });

  test("creation fails if password is less than 3 characters long", async () => {
    const usersAtStart = await usersInDb();

    const newUser = {
      username: "jane",
      name: "Jane",
      email: "jane@gmail.com",
      password: "12",
    };

    const result = await api
      .post("/api/users")
      .send(newUser)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toContain(
      "shorter than the minimum allowed length"
    );

    const usersAtEnd = await usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });
});

describe("when there are users saved", () => {
  test("a user can be returned by id", async () => {
    const users = await usersInDb();

    const result = await api
      .get(`/api/users/${users[0].id}`)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    expect(result.body.username).toContain("testuser");
    expect(result.body.name).toContain("Test");
    expect(result.body.email).toContain("testemail@gmail.com");
  });
});

describe("adding a user to contact list", () => {
  test("succeeds if the id is correct and hasn't been added to the list", async () => {
    const user = {
      username: "testuser",
      password: "sekret",
    };
    const userRes = await api.post("/api/login").send(user);

    const contact = await User.findOne({
      username: { $nin: [user.username] },
    });

    const result = await api
      .put("/api/users/contacts")
      .set("Authorization", "bearer " + userRes.body.token)
      .send({ contactId: contact.id })
      .expect(200)
      .expect("Content-Type", /application\/json/);

    expect(result.body.contactList.length).toEqual(1);
  });

  test("fails if the id is incorrect", async () => {
    const user = {
      username: "testuser",
      password: "sekret",
    };
    const userRes = await api.post("/api/login").send(user);

    const result = await api
      .put("/api/users/contacts")
      .set("Authorization", "bearer " + userRes.body.token)
      .send({ contactId: "626695bd9794dbf773672221" })
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(result.body.error).toEqual("No user with the specified id exists");
  });
});

afterAll(() => {
  mongoose.connection.close();
});

