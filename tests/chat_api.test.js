const mongoose = require("mongoose");
const supertest = require("supertest");
const User = require("../models/user");
const Conversation = require("../models/conversation");
const Message = require("../models/message");
const helper = require("../utils/test_helper");

const app = require("../app");
const api = supertest(app);

//creating new user for other test cases
beforeAll(async () => {
  const newUser = {
    email: "anotheruser@mail.com",
    username: "anotheruser",
    name: "another",
    password: "sekret",
  };
  await api.post("/api/users").send(newUser);
});

beforeEach(async () => {
  await Conversation.deleteMany({});
  await Message.deleteMany({});

  const users = await User.find({});

  let conversation = new Conversation({
    members: [users[0].id, users[1].id],
  });
  conversation = await conversation.save();

  helper.initialMessages.forEach(async (m, i) => {
    //if index is even give the message to first member
    let senderId = i % 2 === 0 ? users[0].id : users[1].id;
    let mObj = new Message({
      content: m,
      senderId,
      conversationId: conversation.id,
    });
    await mObj.save();
  });
});

const login = async () => {
  const user = {
    username: "testuser",
    password: "sekret",
  };
  const res = await api.post("/api/login").send(user);
  return res.body;
};

describe("When a user logs in", () => {
  test("returns the conversations which have the user as member", async () => {
    const user = await login();

    const res = await api
      .get("/api/conversations")
      .set("Authorization", "bearer " + user.token)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const userInDB = await User.findOne({ username: user.username });

    const cMembers = res.body.map((conv) => conv.members);
    cMembers.forEach((members) => {
      expect(members).toContain(userInDB.id);
    });
  });
  test("messages of a conversation are returned", async () => {
    const user = await login();

    let res = await api
      .get("/api/conversations")
      .set("Authorization", "bearer " + user.token);

    res = await api
      .get(`/api/messages/${res.body[0].id}`)
      .set("Authorization", "bearer " + user.token)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    expect(res.body).toHaveLength(helper.initialMessages.length);
  });
});

describe("Starting a new conversation", () => {
  test("succeeds if there is no existing conversation with specified members", async () => {
    const user = await login();

    const receiver = await User.findOne({
      username: "anotheruser",
    });

    await api
      .post("/api/conversations")
      .set("Authorization", "bearer " + user.token)
      .send({ receiver: receiver.id })
      .expect(200);
  });
  test("fails if there is already a conversation with the same members", async () => {
    const user = await login();

    const receiver = await User.findOne({
      username: { $nin: [user.username, "anotheruser"] },
    });

    const res = await api
      .post("/api/conversations")
      .set("Authorization", "bearer " + user.token)
      .send({ receiver: receiver.id })
      .expect(400);

    expect(res.body.error).toContain("Conversation already exists");
  });
});

describe("Adding a new message", () => {
  test("succeeds if user is a member of the conversation", async () => {
    const user = await login();

    let res = await api
      .get("/api/conversations")
      .set("Authorization", "bearer " + user.token);
    const conversationId = res.body[0].id;

    const newMessage = {
      conversationId,
      content: "new message",
    };

    await api
      .post("/api/messages")
      .set("Authorization", "bearer " + user.token)
      .send(newMessage)
      .expect(200)
      .expect("Content-Type", /application\/json/);

    const messages = await Message.find({ conversationId });
    expect(messages).toHaveLength(helper.initialMessages.length + 1);
    expect(messages).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ content: "new message" }),
      ])
    );
    expect(
      messages[helper.initialMessages.length].conversationId.toString()
    ).toEqual(conversationId);
  });
  test("fails if user is not a member of the conversation", async () => {
    let user = await login();
    let res = await api
      .get("/api/conversations")
      .set("Authorization", "bearer " + user.token);
    const conversationId = res.body[0].id;

    user = {
      username: "anotheruser",
      password: "sekret",
    };
    const result = await api.post("/api/login").send(user);

    const newMessage = {
      conversationId,
      content: "new message",
    };
    res = await api
      .post("/api/messages")
      .set("Authorization", "bearer " + result.body.token)
      .send(newMessage)
      .expect(400)
      .expect("Content-Type", /application\/json/);

    expect(res.body.error).toEqual("User not in conversation");
  });
});

afterAll(async () => {
  await User.findOneAndDelete({ username: "anotheruser" });
  mongoose.connection.close();
});
