const http = require("http");
const app = require("./app");
const config = require("./utils/config");

const server = http.createServer(app);

const io = require("socket.io")(server, {
  cors: {
    origin: "https://messaging-frontend-308-309.herokuapp.com",
  },
});

server.listen(config.PORT, () => {
  console.log(`Server running on port ${config.PORT}`);
});

let users = [];

const addUser = (userId, socketId) => {
  !users.some((u) => u.userId === userId) && users.push({ userId, socketId });
};

const removeUser = (socketId) => {
  users = users.filter((u) => u.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((u) => u.userId === userId);
};

io.on("connection", (socket) => {
  //connect
  console.log("A user connected");

  //take userId and socketId from user
  socket.on("newUser", (userId) => {
    addUser(userId, socket.id);
    io.emit("getUsers", users);
  });

  //send and get message
  socket.on("sendMessage", ({ senderId, receiverId, content }) => {
    const user = getUser(receiverId);
    io.to(user.socketId).emit("getMessage", {
      senderId,
      content,
    });
  });

  //when disconnect
  socket.on("disconnect", () => {
    console.log("A user disconnected");
    removeUser(socket.id);
    io.emit("getUsers", users);
  });
});
