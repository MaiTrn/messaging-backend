module.exports = {
  put: {
    description: "Update a user's information",
    operationId: "updateUser",
    security: [{ bearerAuth: [] }],
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              username: {
                type: "string",
                description: "Account username",
                example: "newaccount",
              },
              avatar: {
                type: "string",
                description: "Account's profile picture",
              },
              name: {
                type: "string",
                description: "Account name",
                example: "User",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Account udpated successfully" },
      400: { description: "Update account error" },
    },
  },
};
