module.exports = {
  post: {
    description: "Create user account",
    operationId: "signup",
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              username: {
                type: "string",
                description: "Account username",
                example: "newaccount",
              },
              password: {
                type: "string",
                description: "Account password",
                example: "sekret",
              },
              name: {
                type: "string",
                description: "Account name",
                example: "User",
              },
              email: {
                type: "string",
                description: "Account email",
                example: "user@gmail.com",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Sign up successful" },
      400: { description: "User validation error" },
    },
  },
};
