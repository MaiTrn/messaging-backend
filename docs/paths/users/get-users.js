module.exports = {
  get: {
    description: "Get users",
    operationId: "getUsers",
    parameters: [],
    responses: {
      200: {
        description: "Users obtained",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/user",
            },
          },
        },
      },
    },
  },
};
