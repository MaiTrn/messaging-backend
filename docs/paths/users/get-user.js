module.exports = {
  get: {
    description: "Get a user",
    operationId: "getUser",
    parameters: [
      {
        name: "id",
        in: "path",
        required: true,
        description: "A single user id",
        schema: {
          type: "string",
          description: "An id of a user",
          example: "625d6380160ccdaa6b55a8b8",
        },
      },
    ],
    responses: {
      200: {
        description: "User obtained",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/user",
            },
          },
        },
      },
    },
  },
};
