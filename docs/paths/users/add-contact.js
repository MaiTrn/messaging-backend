module.exports = {
  put: {
    description: "Add contact to list",
    operationId: "addContacts",
    security: [{ bearerAuth: [] }],
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              contactId: {
                type: "string",
                description: "ID of the contact",
                example: "626695bd9794dbf773672221",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Contact added successfully" },
      400: { description: "Contact error" },
    },
  },
};
