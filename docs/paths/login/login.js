module.exports = {
  post: {
    description: "Log in to a user account.",
    operationId: "login",
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              username: {
                type: "string",
                description: "Login username",
                example: "admin",
              },
              password: {
                type: "string",
                description: "Login password",
                example: "sekret",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Log in successfully" },
      400: { description: "User validation error" },
    },
  },
};
