const getUsers = require("./users/get-users");
const signup = require("./users/signup");
const login = require("./login/login");
const getUser = require("./users/get-user");
const getConversations = require("./conversations/get-conversations");
const createConversations = require("./conversations/create-conversations");
const getMessages = require("./messages/get-messages");
const createMessages = require("./messages/create-messages");
const addContacts = require("./users/add-contact");

module.exports = {
  paths: {
    "/api/login": {
      ...login,
    },
    "/api/users": {
      ...getUsers,
      ...signup,
    },
    "/api/users/{id}": {
      ...getUser,
    },
    "/api/users/contacts": {
      ...addContacts,
    },
    "/api/conversations": {
      ...getConversations,
      ...createConversations,
    },
    "/api/messages": {
      ...createMessages,
    },
    "/api/messages/{id}": {
      ...getMessages,
    },
  },
};

