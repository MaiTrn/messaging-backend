module.exports = {
  get: {
    description: "Get conversations of a user",
    operationId: "getConversations",
    security: [{ bearerAuth: [] }],
    parameters: [],
    responses: {
      200: {
        description: "Conversations obtained",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/conversation",
            },
          },
        },
      },
      401: {
        description: "Token missing or invalid",
      },
    },
  },
};
