module.exports = {
  post: {
    description: "Create conversation of a user",
    operationId: "createConversations",
    security: [{ bearerAuth: [] }],
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              receiver: {
                type: "string",
                description: "ID of the receiver",
                example: "625d63da9e9d00cf82a83400",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Conversation created" },
      400: { description: "Conversation already exists" },
      401: { description: "Token missing or invalid" },
    },
  },
};
