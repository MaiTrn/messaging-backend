module.exports = {
  get: {
    description: "Get messages of a conversation",
    operationId: "getMessages",
    security: [{ bearerAuth: [] }],
    parameters: [
      {
        name: "id",
        in: "path",
        required: true,
        description: "A single conversation id",
        schema: {
          type: "string",
          description: "An id of a conversation",
          example: "625d688e1024175e8fca84dd",
        },
      },
    ],
    responses: {
      200: {
        description: "Messages obtained",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/message",
            },
          },
        },
      },
      401: {
        description: "Token missing or invalid",
      },
    },
  },
};
