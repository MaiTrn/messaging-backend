module.exports = {
  post: {
    description: "Create message from a user",
    operationId: "createMessages",
    security: [{ bearerAuth: [] }],
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              conversationId: {
                type: "string",
                description: "ID of the conversation",
                example: "625d688e1024175e8fca84dd",
              },
              content: {
                type: "string",
                description: "Message content",
                example: "Hello from swagger",
              },
            },
          },
        },
      },
    },
    responses: {
      200: { description: "Message created" },
      400: { description: "User not in conversation" },
      401: { description: "Token missing or invalid" },
    },
  },
};
