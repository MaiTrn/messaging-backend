module.exports = {
  components: {
    schemas: {
      //user model
      user: {
        type: "object",
        properties: {
          name: {
            type: "string",
            description: "The user's name",
            example: "John Doe",
          },
          username: {
            type: "string",
            description: "Username of the account",
            example: "johndoe",
          },
          email: {
            type: "string",
            description: "Email of the account",
            example: "johndoe@email.com",
          },
          avatar: {
            type: "string",
            description: "The avatar uri uploaded onto cloudinary",
          },
          passwordHash: {
            type: "string",
            description: "Hashed version of the account's password",
          },
          contactList: {
            type: "array",
            description: "User's contact list",
          },
        },
      },
      conversation: {
        type: "object",
        properties: {
          members: {
            type: "array",
            description:
              "user ids of the members participating in the conversation",
          },
        },
      },
      message: {
        type: "object",
        properties: {
          conversationId: {
            type: "string",
            description: "Reference to the conversation's id",
          },
          senderId: {
            type: "string",
            description: "Reference to the sender's id",
          },
          content: {
            type: "string",
            description: "Content of the message",
          },
        },
      },
    },
    securitySchemes: {
      bearerAuth: {
        type: "http",
        description: "An auth token of a user",
        scheme: "bearer",
        name: "auth",
        in: "header",
      },
    },
  },
};
