module.exports = {
  openapi: "3.0.3",
  info: {
    title: "Messaging API",
    description: "Messaging API for chat application",
    version: "1.0.0",
  },
};
